﻿using System;
using System.Dynamic;
using System.Windows.Forms;
using System.IO.Ports;

namespace DMXWriter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            foreach (string sp in SerialPort.GetPortNames())
            {
                comboBox_COMPort.Items.Add(sp);
            }

            if (comboBox_COMPort.Items.Count > 0)
            {
                comboBox_COMPort.SelectedIndex = 0;
            }
            else
            {
                MessageBox.Show(@"No COM Ports found. Exit and restart to scan again.");
            }
        }

        private byte MSB(int x)
        {
            return (byte) ((x >> 8) & 0xFF);
        }

        private byte LSB(int x)
        {
            return (byte) (x & 0xFF);
        }

        private void button_Write_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            const int channels = 3;
            byte[] headerfooter = {195, 15, 99, 80, 6, 163, 245, 136, 60};

            byte[] data = {195, 0, 0, 0, 0, 255, 0, 0, 60};

            int x = ((int) numericUpDown_Address.Value - 1) * channels + 1;
            byte check = (byte) ((MSB(x) + LSB(x) + MSB(channels) +
                                  LSB(channels) + 255 + 40) % 256);

            data[1] = MSB(x);
            data[2] = LSB(x);
            data[3] = MSB(channels);
            data[4] = LSB(channels);
            data[7] = check;

            SerialConnection sc = new SerialConnection((string) comboBox_COMPort.SelectedItem);

            if (!sc.IsOpen)
            {
                MessageBox.Show(this, @"Unable to open serial port.", @"Error");
            }
            else
            {
                System.Threading.Thread.Sleep(2000);
                sc.Send(headerfooter);
                System.Threading.Thread.Sleep(199);
                sc.Send(data);
                System.Threading.Thread.Sleep(199);
                sc.Send(headerfooter);

                const int dmxheader = 5;
                const int dmxfooter = 1;

                byte[] dmxdata = new byte[dmxheader + 513 + dmxfooter];

                // R - G - B - W - G
                DmxInitialise(ref dmxdata);
                dmxdata[dmxheader + (int)numericUpDown_Address.Value] = 255;
                sc.Send(dmxdata);
                System.Threading.Thread.Sleep(1000);
                DmxInitialise(ref dmxdata);
                dmxdata[dmxheader + (int)numericUpDown_Address.Value+1] = 255;
                sc.Send(dmxdata);
                System.Threading.Thread.Sleep(1000);
                DmxInitialise(ref dmxdata);
                dmxdata[dmxheader + (int)numericUpDown_Address.Value+2] = 255;
                sc.Send(dmxdata);
                System.Threading.Thread.Sleep(1000);
                DmxInitialise(ref dmxdata);
                dmxdata[dmxheader + (int)numericUpDown_Address.Value] = 255;
                dmxdata[dmxheader + (int)numericUpDown_Address.Value + 1] = 255;
                dmxdata[dmxheader + (int)numericUpDown_Address.Value + 2] = 255;
                sc.Send(dmxdata);
                System.Threading.Thread.Sleep(1000);
                DmxInitialise(ref dmxdata);
                dmxdata[dmxheader + (int)numericUpDown_Address.Value + 1] = 255;
                sc.Send(dmxdata);
            }

            sc.Destroy();

            Cursor = Cursors.Arrow;
        }

        // Looking at this code copied from xLights I am concerned
        // I suspect the dongle eats this and then writes 0 + data
        void DmxInitialise(ref byte[] dmxdata)
        {
            dmxdata.Initialize();
            dmxdata[0] = 0x7E;
            dmxdata[1] = 0x06;
            dmxdata[2] = 0x00;
            dmxdata[3] = 0x01;
            dmxdata[4] = 0x00;
            dmxdata[dmxdata.Length - 1] = 0x7E;
        }
    }

    class SerialConnection
    {
        private SerialPort _port = new SerialPort();

        public SerialConnection(string port)
        {
            _port.BaudRate = 250000;
            _port.DataBits = 8;
            _port.Parity = Parity.None;
            _port.StopBits = StopBits.Two;
            _port.PortName = port;

            _port.Open();
        }

        ~SerialConnection()
        {
            Destroy();
        }

        public void Destroy()
        {
            if (_port.IsOpen)
            {
                _port.Close();
            }
        }

        public bool IsOpen
        {
            get { return _port.IsOpen; }
        }

        public void Send(byte[] data)
        {
            _port.Write(data, 0, data.Length);
        }
    }
}
