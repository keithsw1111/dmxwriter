﻿namespace DMXWriter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown_Address = new System.Windows.Forms.NumericUpDown();
            this.button_Write = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_COMPort = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Address)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "DMX Address";
            // 
            // numericUpDown_Address
            // 
            this.numericUpDown_Address.Location = new System.Drawing.Point(112, 47);
            this.numericUpDown_Address.Maximum = new decimal(new int[] {
            512,
            0,
            0,
            0});
            this.numericUpDown_Address.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_Address.Name = "numericUpDown_Address";
            this.numericUpDown_Address.Size = new System.Drawing.Size(120, 22);
            this.numericUpDown_Address.TabIndex = 2;
            this.numericUpDown_Address.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // button_Write
            // 
            this.button_Write.Location = new System.Drawing.Point(85, 84);
            this.button_Write.Name = "button_Write";
            this.button_Write.Size = new System.Drawing.Size(75, 23);
            this.button_Write.TabIndex = 4;
            this.button_Write.Text = "Write";
            this.button_Write.UseVisualStyleBackColor = true;
            this.button_Write.Click += new System.EventHandler(this.button_Write_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "COM Port";
            // 
            // comboBox_COMPort
            // 
            this.comboBox_COMPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_COMPort.FormattingEnabled = true;
            this.comboBox_COMPort.Location = new System.Drawing.Point(112, 12);
            this.comboBox_COMPort.Name = "comboBox_COMPort";
            this.comboBox_COMPort.Size = new System.Drawing.Size(121, 24);
            this.comboBox_COMPort.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 120);
            this.Controls.Add(this.comboBox_COMPort);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button_Write);
            this.Controls.Add(this.numericUpDown_Address);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "DMX Writer";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Address)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown_Address;
        private System.Windows.Forms.Button button_Write;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox_COMPort;
    }
}

